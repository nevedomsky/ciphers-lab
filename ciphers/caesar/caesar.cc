// Copyright @nevack 2020.

#include "caesar.h"

#include <iostream>

namespace ciphers {

Caesar::~Caesar() = default;

Caesar::Caesar(const string_t& alphabet, int shift)
    : AlphabetCipher(alphabet), shift_(shift) {}

string_t Caesar::Encode(const string_t& message) {
  return encode_impl(message, true);
}

string_t Caesar::Decode(const string_t& message) {
  return encode_impl(message, false);
}

string_t Caesar::encode_impl(const string_t& message, bool encode) {
  string_t out;

  for (const auto& c : message) {
    if (!IsInAlphabet(c)) {
      out += c;
      continue;
    }

    const int index = encode ? BoundedIndexOf(IndexOf(c) + shift_)
                             : BoundedIndexOf(IndexOf(c) - shift_);

    out += LetterOf(index);
  }

  return out;
}

}  // namespace ciphers
