// Copyright @nevack 2020.

#include "gtest/gtest.h"
#include "caesar.h"

namespace ciphers {

namespace {

constexpr const char_t kMessage[] = L"BABЖ CD, E1...";

}

TEST(CaesarTest, Encode) {
  Caesar cipher(L"12Kd", -2);
  EXPECT_EQ(cipher.Encode(L"22KРDя121K"), L"dd1РDяKdK1");
}

TEST(CaesarTest, Decode) {
  Caesar cipher(L"LADJ1212Kd", 30);
  EXPECT_EQ(cipher.Decode(L"DNJhdj asjd hA:DK,asd ad."),
                          L"1N2hAj asjA hJ:1L,asA aA.");
}

TEST(CaesarTest, EncodeDecode) {
  Caesar cipher(L"ABCDE", 7);
  EXPECT_EQ(cipher.Decode(cipher.Encode(kMessage)), kMessage);
}

}  // namespace ciphers
