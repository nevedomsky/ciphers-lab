// Copyright @nevack 2020.

#ifndef __CAESAR_H__
#define __CAESAR_H__

#include "ciphers/cipher/alphabet_cipher.h"

namespace ciphers {

class Caesar : public AlphabetCipher {
 public:
  ~Caesar() override;
  Caesar(const string_t& alphabet, int shift);

  string_t Encode(const string_t& message) override;
  string_t Decode(const string_t& message) override;

 private:
  string_t encode_impl(const string_t& message, bool encode);

  const int shift_;
};

}  // namespace ciphers

#endif // __CAESAR_H__
