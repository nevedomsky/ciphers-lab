// Copyright @nevack 2020.

#include <iostream>

#include "ciphers/caesar/caesar.h"

namespace caesar::errors {

struct Error {
    const char* msg;
    const int id;
};

constexpr const char kHelpMessage[] = R"(
Caesar Cipher Usage:
  caesar <shift> - [de|en]code text from stdin to stdout
  caesar <shift> infile outfile - use files instead of std

  <shift> - integer, positive for encode and negative for decode
)";

constexpr const Error kShiftError { kHelpMessage, 1 };

constexpr const char kShiftErrorMessage[] = R"(
Shift must be a non-zero number.
)";

constexpr const char* kErrors[] {
    kHelpMessage,
    kShiftErrorMessage
};

}  // namespace caesar::errors

int main(int argc, char const* argv[]) {
  if (!argc-- || (argc != 1 && argc != 3)) {
    std::cout << caesar::errors::kHelpMessage;
    return 1;
  }

  int shift = 0;

  try {
    shift = std::stoi(argv[1]);
  } catch (...) {
    std::cout << caesar::errors::kShiftErrorMessage;
    return 2;
  }

  if (shift == 0) {
    std::cout << caesar::errors::kShiftErrorMessage;
    return 3;
  }


  std::cout << shift << std::endl;
}