// Copyright @nevack 2020.

#ifndef __VIGENERE_H__
#define __VIGENERE_H__

#include "ciphers/cipher/alphabet_cipher.h"

namespace ciphers {

class Vigenere : public AlphabetCipher {
 public:
  ~Vigenere() override;
  Vigenere(const string_t& alphabet, const string_t& keyword);

  string_t Encode(const string_t& message) override;
  string_t Decode(const string_t& message) override;

 private:
  string_t encode_impl(const string_t& message, bool encode);

  const string_t keyword_;
};

}  // namespace ciphers

#endif // __VIGENERE_H__
