// Copyright @nevack 2020.

#include "gtest/gtest.h"
#include "vigenere.h"

namespace ciphers {

namespace {

constexpr const char_t kMessage[] = L"BABЖ CD, E1...";

}

TEST(VigenereTest, Encode) {
  Vigenere cipher(L"ABCDEFGHIJKLMNOPQRSTUVWXYZ", L"LEMON");
  EXPECT_EQ(cipher.Encode(L"ATTACKATDAWN!"), L"LXFOPVEFRNHR!");
}

TEST(VigenereTest, Decode) {
  Vigenere cipher(L"ABCDEFGHIJKLMNOPQRSTUVWXYZ", L"LEMON");
  EXPECT_EQ(cipher.Decode(L"LXFOPV EF RNHR"), L"ATTACK AT DAWN");
}

TEST(VigenereTest, EncodeDecode) {
  Vigenere cipher(L"ABCDEK", L"KEK");
  EXPECT_EQ(cipher.Decode(cipher.Encode(kMessage)), kMessage);
}

}  // namespace ciphers
