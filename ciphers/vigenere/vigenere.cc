// Copyright @nevack 2020.

#include "vigenere.h"

#include <sstream>
#include <stdexcept>

namespace ciphers {

Vigenere::~Vigenere() = default;

Vigenere::Vigenere(const string_t& alphabet, const string_t& keyword)
    : AlphabetCipher(alphabet), keyword_(keyword) {
  for (const auto& c : keyword_) {
    if (IsInAlphabet(c)) {
      continue;
    }
    std::stringstream message;
    message << "Keyword char " << c << " is out of alphabet";
    throw std::invalid_argument(std::move(message.str()));
  }
}

string_t Vigenere::Encode(const string_t& message) {
  return encode_impl(message, true);
}

string_t Vigenere::Decode(const string_t& message) {
  return encode_impl(message, false);
}

string_t Vigenere::encode_impl(const string_t& message, bool encode) {
  string_t out;

  std::size_t keyword_index = 0;

  for (const auto& c : message) {
    if (!IsInAlphabet(c)) {
      out += c;
      continue;
    }

    const int m = IndexOf(c);
    const int k = IndexOf(keyword_[keyword_index]);

    const int index = encode ? BoundedIndexOf(m + k)
                             : BoundedIndexOf(m - k);

    out += LetterOf(index);

    keyword_index++;
    if (keyword_index == keyword_.size()) {
      keyword_index = 0;
    }
  }

  return out;
}

}  // namespace ciphers
