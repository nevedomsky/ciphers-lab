// Copyright @nevack 2020.

#include <fstream>
#include <iostream>

#include "ciphers/caesar/caesar.h"

int main() {
  ciphers::Caesar cipher(L"ABCDE", 2);

  std::wcout << cipher.Encode(L"AB AE, \n");

  std::wcout << cipher.Decode(cipher.Encode(L"ABABCDE")) << std::endl;
}