// Copyright @nevack 2020.

#ifndef __CIPHER_H__
#define __CIPHER_H__

#include <vector>
#include <string>
#include <unordered_map>

namespace ciphers {

using char_t = wchar_t;
using string_t = std::basic_string<char_t>;

template<typename String = string_t>
class Cipher {
 public:
  virtual ~Cipher() = default;

  virtual String Decode(const String& message) = 0;
  virtual String Encode(const String& message) = 0;
};

}  // namespace ciphers

#endif // __CIPHER_H__
