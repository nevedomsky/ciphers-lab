// Copyright @nevack 2020.

#include "alphabet_cipher.h"

namespace ciphers {

AlphabetCipher::~AlphabetCipher() = default;

AlphabetCipher::AlphabetCipher(const string_t& text) {
  int index = 0;
  for (const auto& c : text) {
    if (!IsInAlphabet(c)) {
      alphabet_.push_back(c);
      uniquety_checker_[c] = index++;
    }
  }
}

bool AlphabetCipher::IsInAlphabet(char_t c) {
  return uniquety_checker_.find(c) != uniquety_checker_.end();
}

int AlphabetCipher::IndexOf(char_t c) {
  return uniquety_checker_[c];
}

char_t AlphabetCipher::LetterOf(int index) {
  return alphabet_[index];
}

int AlphabetCipher::BoundedIndexOf(int unbounded_index) {
  unbounded_index %= static_cast<int>(alphabet_.size());
  if (unbounded_index < 0)
    unbounded_index += alphabet_.size();

  return unbounded_index;
}

}  // namespace ciphers