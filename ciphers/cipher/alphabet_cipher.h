// Copyright @nevack 2020.

#ifndef __ALPHABET_CIPHER_H__
#define __ALPHABET_CIPHER_H__

#include <vector>
#include <unordered_map>

#include "cipher.h"

namespace ciphers {

class AlphabetCipher : public Cipher<> {
 public:
  ~AlphabetCipher() override;
 protected:
  AlphabetCipher(const string_t& alphabet);

  bool IsInAlphabet(char_t c);
  int IndexOf(char_t c);
  char_t LetterOf(int index);
  int BoundedIndexOf(int unbounded_index);

 private:
  std::unordered_map<char_t, int> uniquety_checker_;
  std::vector<char_t> alphabet_;
};

}  // namespace ciphers

#endif // __ALPHABET_CIPHER_H__
