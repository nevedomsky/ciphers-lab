

#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <unordered_map>
#include <utility>

#include "des/des.h"

const char kAS_TGS[] = "AS->TGS";
const char kTGS_SS[] = "TGS->SS";
const char kTGS[] = "TGS-1";
const int kIntervalMs = 5000;

long long time_ms() {
  auto time = std::chrono::system_clock::now().time_since_epoch();
  return std::chrono::duration_cast<std::chrono::milliseconds>(time).count();
}

std::vector<std::string> split(const std::string& s, char delim) {
  std::vector<std::string> result;
  std::stringstream ss(s);
  std::string item;

  while (std::getline(ss, item, delim)) {
    result.push_back(item);
  }

  return result;
}

class Server {
 public:
  Server(const std::string& name) : name_(name) {}
  virtual ~Server() = default;

  const std::string& name() { return name_; }

  virtual std::optional<std::string> handle(const std::string& message) = 0;

 private:
  const std::string name_;
};

class AuthServer : public Server {
 public:
  AuthServer(const std::string& name) : Server(name) {
    clients_["nevack"] = {"KEKLOL", "TICKETAUTHFORNEVACK"};
  }
  ~AuthServer() override = default;

  std::optional<std::string> handle(const std::string& id) override {
    auto client_it = clients_.find(id);
    if (client_it == clients_.end()) {
      std::cout << "AUTH - User not found: " << id << std::endl;
      return std::nullopt;
    }

    const auto& [kc, tgs_kc] = client_it->second;

    des::DES client(kc), tgs(kAS_TGS);

    std::string s = id + ";" + kTGS + ";" + std::to_string(time_ms()) + ";" +
                    std::to_string(kIntervalMs) + ";" + tgs_kc;
    std::cout << "AUTH - Sending ticket:   " << s << std::endl;
    s = tgs.Encode(s);
    s += ";" + tgs_kc;
    s = client.Encode(s);

    return s;
  }

 private:
  std::unordered_map<std::string, std::pair<std::string, std::string>> clients_;
};

class TicketServer : public Server {
 public:
  TicketServer(const std::string& name) : Server(name) {
    clients_["nevack"] = "NEVACKSS";
  }
  ~TicketServer() override = default;

  std::optional<std::string> handle(const std::string& message) override {
    des::DES tgt(kAS_TGS);
    int pos = message.size();
    while (--pos > 0) {
      if (message[pos] == ';') {
        break;
      }
    }

    std::string server_id(message.begin() + (pos + 1), message.end());

    int prev_pos = pos;
    while (--pos > 0) {
      if (message[pos] == ';') {
        break;
      }
    }

    std::string aut(message.begin() + (pos + 1), message.begin() + prev_pos);

    std::string tgt_dec(message.begin(), message.begin() + pos + 2);
    tgt_dec = tgt.Decode(tgt_dec);
    std::cout << " TGT - Received ticket:  " << tgt_dec << std::endl;

    auto parts = split(tgt_dec, ';');

    auto client = parts[0];
    auto server = parts[1];
    auto start = std::stol(parts[2]);
    auto duration = std::stol(parts[3]);
    des::DES tgs(parts[4]);

    if (server != kTGS) {
      std::cout << " TGS - Server mismatch: " << server << " vs. " << kTGS
                << std::endl;
      return std::nullopt;
    }

    aut = tgs.Decode(aut);
    std::cout << " TGS -       with AUTH:  " << aut << std::endl;

    parts = split(aut, ';');
    aut = parts[0];
    if (aut != client) {
      std::cout << " TGS - Client mismatch: " << client << " vs. " << aut
                << std::endl;
      return std::nullopt;
    }

    auto end = std::stol(parts[1]);

    if (end - start > duration) {
      std::cout << " TGS - Ticket is outdated: " << duration << " > "
                << (end - start) << std::endl;
      return std::nullopt;
    }

    auto client_it = clients_.find(client);
    if (client_it == clients_.end()) {
      std::cout << " TGS - User not found: " << client << std::endl;
      return std::nullopt;
    }

    des::DES ss(kTGS_SS);
    std::string s = client + ";" + server_id + ";" + std::to_string(time_ms()) +
                    ";" + std::to_string(duration) + ";" + client_it->second;
    std::cout << " TGS - Sending ticket:   " << s << std::endl;
    s = ss.Encode(s);
    s += ";" + client_it->second;
    s = tgs.Encode(s);

    return s;
  }

 private:
  std::unordered_map<std::string, std::string> clients_;
};

class SomeServer : public Server {
 public:
  SomeServer(const std::string& name) : Server(name) {}
  ~SomeServer() override = default;
  std::optional<std::string> handle(const std::string& message) override {
    des::DES tgt(kTGS_SS);
    int pos = message.size();
    while (--pos > 0) {
      if (message[pos] == ';') {
        break;
      }
    }

    std::string server_id(message.begin() + (pos + 1), message.end());

    int prev_pos = pos;
    while (--pos > 0) {
      if (message[pos] == ';') {
        break;
      }
    }

    std::string aut(message.begin() + (pos + 1), message.begin() + prev_pos);

    std::string tgt_dec(message.begin(), message.begin() + pos + 2);
    tgt_dec = tgt.Decode(tgt_dec);
    std::cout << "  SS - Received ticket:  " << tgt_dec << std::endl;

    auto parts = split(tgt_dec, ';');

    auto client = parts[0];
    auto server = parts[1];
    auto start = std::stol(parts[2]);
    auto duration = std::stol(parts[3]);
    des::DES tgs(parts[4]);

    if (server_id != "SS") {
      std::cout << "  SS - Server mismatch: " << server_id << " vs. "
                << "SS" << std::endl;
      return std::nullopt;
    }

    aut = tgs.Decode(aut);
    std::cout << "  SS -       with AUTH:  " << aut << std::endl;

    parts = split(aut, ';');
    aut = parts[0];
    if (aut != client) {
      std::cout << " TGS - Client mismatch: " << client << " vs. " << aut
                << std::endl;
      return std::nullopt;
    }

    auto end = std::stol(parts[1]);

    if (end - start > duration) {
      std::cout << "  SS - Ticket is outdated: " << (end - start) << " > "
                << duration << std::endl;
      return std::nullopt;
    }

    std::string s = std::to_string(end + 1);
    std::cout << "  SS - Sending timestamp:   " << s << std::endl;
    s = tgs.Encode(s);

    return s;
  }
};

int main() {
  des::DES nevack("KEKLOL");
  std::string id = "nevack";

  AuthServer auth("AUTH");
  TicketServer tickets("TGT");
  SomeServer server("SS");

  auto auth_ans = auth.handle(id);

  if (!auth_ans) {
    std::cout << "USER - Auth failed.\n";
    return 0;
  }

  auto ans_dec = nevack.Decode(*auth_ans);

  int pos = ans_dec.size();
  while (--pos > 0) {
    if (ans_dec[pos] == ';') {
      break;
    }
  }

  std::string tgs(ans_dec.begin() + (pos + 1), ans_dec.end());
  std::cout << "USER - Received TGS key: " << tgs << std::endl;
  des::DES tgsDES(tgs);

  std::string aut = id + ";" + std::to_string(time_ms());
  aut = tgsDES.Encode(aut);

  std::string tgt = std::string(ans_dec.begin(), ans_dec.begin() + pos) + ";" +
                    aut + ";" + "SS";

  auto tgs_ans = tickets.handle(tgt);

  if (!tgs_ans) {
    std::cout << "USER - Obtaining ticket failed.\n";
    return 0;
  }

  auto tgs_dec = tgsDES.Decode(*tgs_ans);

  pos = tgs_dec.size();
  while (--pos > 0) {
    if (tgs_dec[pos] == ';') {
      break;
    }
  }

  std::string ss(tgs_dec.begin() + (pos + 1), tgs_dec.end());
  std::cout << "USER - Received SS key:  " << ss << std::endl;
  des::DES ssDES(ss);

  std::this_thread::sleep_for(std::chrono::milliseconds(kIntervalMs - 100));

  auto time = time_ms();
  aut = id + ";" + std::to_string(time);
  aut = ssDES.Encode(aut);
  ss = std::string(tgs_dec.begin(), tgs_dec.begin() + pos) + ";" + aut + ";" +
       "SS";

  auto ss_ans = server.handle(ss);

  if (!ss_ans) {
    std::cout << "USER - Communication failed.\n";
    return 0;
  }

  auto ss_dec = ssDES.Decode(*ss_ans);
  std::cout << "USER - Received timestamp:  " << ss_dec << std::endl;

  if (std::stol(ss_dec) - 1 != time) {
    std::cout << "USER - Timestamp mismatch: " << ss_dec << " vs. " << time
              << std::endl;
    return 0;
  }

  std::cout << "USER - Communication verified!\n";
}