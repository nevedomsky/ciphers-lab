// Copyright @nevack 2020.

#include "gtest/gtest.h"
#include "des/des.h"

namespace des {

namespace {

constexpr const char kMessage[] = "Hello, World!";

}

TEST(DESTest, Encode) {
  DES des("KEKLOL");
  EXPECT_EQ(des.Decode(des.Encode(kMessage)), kMessage);
}

}  // namespace ciphers
