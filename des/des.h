// Copyright @nevack 2020.

#ifndef __DES_H__
#define __DES_H__

#include <array>

#include "ciphers/cipher/cipher.h"

namespace des {

template<std::size_t N>
using Bits = std::array<bool, N>;
using Block = Bits<64>;
using Data = Bits<32>;
using Key = Bits<56>;

using String = std::string;

class DES : public ciphers::Cipher<String> {
 public:
  DES(const std::string& key);
  ~DES() override;

  String Decode(const String& message) override;
  String Encode(const String& message) override;

 private:
  Block key_ = {{}};
  std::array<Key, 16> k_ = {{}};

  Data f(const Data& data, const Key& key);

  Block encode(const Block& block);
  Block decode(const Block& block);
};

}  // namespace des

#endif // __DES_H__
