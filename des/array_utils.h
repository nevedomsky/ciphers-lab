// Copyright @nevack 2020.

#ifndef __ARRAY_UTILS_H__
#define __ARRAY_UTILS_H__

#include <array>
#include <iostream>

template <class T, std::size_t N>
void println(const std::array<T, N>& in, const char sep = 0,
             const int group = 0) {
  if (sep) {
    if (group) {
      int x = 0;
      for (std::size_t i = 0; i < N; ++i) {
        std::cout << in[i];
        if (++x == group) {
          std::cout << sep;
          x = 0;
        }
      }
    } else {
      for (std::size_t i = 0; i < N; ++i) {
        std::cout << in[i] << sep;
      }
    }
  } else {
    for (std::size_t i = 0; i < N; ++i) {
      std::cout << in[i];
    }
  }
  std::cout << '\n';
}

template <class T, std::size_t N, std::size_t M>
void println(const std::array<std::array<T, N>, M>& in) {
  for (std::size_t i = 0; i < M; ++i) {
    println(in[i]);
  }
}

template <class T, std::size_t N1, std::size_t N2>
std::array<T, N2> permute(const std::array<T, N1>& in,
                          const std::array<int, N2>& order) {
  std::array<T, N2> out;
  std::size_t out_index = 0;
  for (const auto& in_index : order) {
    out[out_index++] = in[in_index - 1];
  }

  return out;
}

template <class T, std::size_t N>
std::array<T, N> rotate_left(const std::array<T, N>& in, const int n) {
  std::array<T, N> out;
  for (std::size_t i = 0; i < N; ++i) {
    out[i] = in[(i + n) % N];
  }

  return out;
}

template <class T, std::size_t N>
std::array<T, N> rotate_right(const std::array<T, N>& in, const int n) {
  std::array<T, N> out;
  for (std::size_t i = 0; i < N; ++i) {
    int index = ((int)i - n) % N;
    if (index < 0) index += N;
    out[i] = in[index];
  }

  return out;
}

template <class T, std::size_t N1, std::size_t N2>
std::array<T, N1 + N2> concat(const std::array<T, N1>& a,
                              const std::array<T, N2>& b) {
  std::array<T, N1 + N2> out;
  for (std::size_t i = 0; i < N1; ++i) {
    out[i] = a[i];
  }
  for (std::size_t i = 0; i < N2; ++i) {
    out[i + N1] = b[i];
  }

  return out;
}

template <class T, std::size_t N, std::size_t P = N / 2,
          std::enable_if_t<N % 2 == 0, int> = 0>
std::pair<std::array<T, P>, std::array<T, N - P>> split(
    const std::array<T, N>& in) {
  std::array<T, P> l;
  std::array<T, N - P> r;
  for (std::size_t i = 0; i < P; ++i) {
    l[i] = in[i];
  }
  for (std::size_t i = P; i < N; ++i) {
    r[i - P] = in[i];
  }

  return {l, r};
}

template <class T, std::size_t N1, std::size_t N2,
          std::size_t N = std::min(N1, N2)>
std::array<T, N> arr_xor(const std::array<T, N1>& a,
                         const std::array<T, N2>& b) {
  std::array<T, N> out;
  for (std::size_t i = 0; i < N; ++i) {
    out[i] = a[i] ^ b[i];
  }

  return out;
}

template <std::size_t X, class T, std::size_t N, std::size_t M = N / X,
          std::enable_if_t<N % X == 0, int> = 0>
std::array<std::array<T, X>, M> chunked(const std::array<T, N>& in) {
  std::array<std::array<T, X>, M> out;
  for (std::size_t i = 0; i < M; ++i) {
    for (std::size_t j = 0; j < X; ++j) {
      out[i][j] = in[i * M + j];
    }
  }

  return out;
}

#endif  // __ARRAY_UTILS_H__
