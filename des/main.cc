
#include <iostream>

#include "des/des.h"

const char kLipsum[] =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla interdum "
    "mollis gravida. Proin efficitur porta dolor. Phasellus volutpat id tortor "
    "id bibendum. Sed volutpat nisi ut orci blandit  .";

int main() {
  des::DES des("PRIVET?!");
  std::cout << "SRC: " << kLipsum << " [" << std::size(kLipsum) << "]\n";
  auto enc = des.Encode(kLipsum);
  std::cout << "ENC: " << enc << " [" << std::size(enc) << "]\n";
  auto dec = des.Decode(enc);
  std::cout << "DEC: " << dec << " [" << std::size(dec) << "]\n";
}